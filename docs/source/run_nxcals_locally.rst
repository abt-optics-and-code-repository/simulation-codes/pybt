How to run NXCALS locally on MacOS
===================================

This is a guide on how to run NXCALS locally on MacOS and using visual studio code (instead of SWAN).

I followed mostly this `documentation <https://nxcals-docs.web.cern.ch/current/user-guide/data-access/quickstart/>`_ and also the `data extraction API <https://nxcals-docs.web.cern.ch/current/user-guide/extraction-api/>`_

Make sure you requested access to NXCALS.

Virtual environment
-------------------

I recommend to use acc-py, it takes care of using CERN's repo and comes preinstalled with nxcals.
Be careful, If one uses, python 3.11 shipped with acc-py 2023 is not compatible until sometime this month when spark will be upgraded to 3.5. Until then acc-py 2021 needs to be used.
See `Acc-py <https://mattermost.web.cern.ch/abt-operation/pl/xszzx89bbbgxupwj11117djbmo>`_

Manually installation without acc-py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need to create a venv and pip install nxcals

.. code-block:: bash

    python3 -m venv ./venv source ./venv/bin/activate python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch nxcals

Java
----

Check your java version, you need java >11

.. code-block:: bash

    java -version

Install pyarrow

.. code-block:: bash

    pip install pyarrow

Kerberos
--------

You need to initialise kerberos

.. code-block:: bash

    kinit

or 

.. code-block::

    kinit eljohnso@CERN.CH

For this you need a configuration file found (or created if not there) in */etc/krb5.conf* (MacOS)

.. code-block:: bash

    [libdefaults]
        ticket_lifetime = 1560m
        #default_realm = FNAL.GOV
        default_realm = CERN.CH
        ccache_type = 4
        default_tgs_enctypes =  aes256-cts-hmac-sha1-96  aes128-cts-hmac-sha1-96 des3-cbc-sha1  des-cbc-crc arcfour-hmac-md5
        default_tkt_enctypes =  aes256-cts-hmac-sha1-96  aes128-cts-hmac-sha1-96 des3-cbc-sha1  des-cbc-crc arcfour-hmac-md5
        permitted_enctypes = aes256-cts-hmac-sha1-96  aes128-cts-hmac-sha1-96 des3-cbc-sha1  des-cbc-crc arcfour-hmac-md5
        default_lifetime = 7d
        renew_lifetime = 7d
        autologin = true
        forward = true
        forwardable = true
        renewable = true
        encrypt = true
        allow_weak_crypto = true
        chpw_prompt = true

    [realms]
        CERN.CH = {
                        kdc = cerndc.cern.ch:88
                        master_kdc = cerndc.cern.ch:88
                        default_domain = cern.ch
                        kpasswd_server = afskrb5m.cern.ch
                        #kpasswd_server = cerndc.cern.ch
                        admin_server = afskrb5m.cern.ch
                        #admin_server = cerndc.cern.ch
                        v4_name_convert = {
                                host = {
                                        rcmd = host
                                }
                        }
                }
    [domain_realm]
        .cern.ch = CERN.CH
        lxplus.cern.ch = CERN.CH

SPARK object
--------------------

The cluster connection can be created in 2 different ways detailed `here <https://nxcals-docs.web.cern.ch/current/user-guide/spark-session/>`_: **LOCAL** or **YARN** mode

Create a spark session in local mode. What you need to retrieve data is this *spark* object:

* My understanding is that in local mode the orchestrator is running on your local machine, this means a lot more data needs to pass by your machine than what you request.

.. code-block:: python

    from nxcals import spark_session_builder
    spark = SparkSession.builder \
        .appName("MY_APP") \
        .config("spark.driver.memory", "4g") \
        .config("spark.executor.memory", "4g") \
        .getOrCreate()

[Optional] YARN mode
--------------------

You can also create a spark session in YARN mode which create a remote session with virtual cores to handle the data extraction (but for me it takes 15 min to run the cell). This requires Python 3.7 or 3.9 (to install `Download macOS 64-bit universal2 installer <https://www.python.org/downloads/macos/>`_ :

.. code-block:: python

    python3.9 -m venv your_env_name

In YARN mode the local environment is packaged and shipped to the cluster, this is a waste because my local environment is very heavy compared to what I need on the cluster for my UDFs but I didn't manage to use another one just yet because PYSPARK_PYTHON somehow gets overridden
For large dataset the YARN mode is necessary but I personally have recurrent issues

.. code-block:: python

    from nxcals.spark_session_builder import get_or_create, Flavor 
    # Using Flavor.YARN_SMALL, all properties pre-set but can be overwritten, running on Yarn
    spark = get_or_create(app_name='MY_APP', flavor=Flavor.YARN_LARGE,
                                  conf={'spark.executor.instances': '6'})

You can also tweak the configuration file:

.. code-block:: bash

    conf={
          "spark.driver.cores": "4",
          "spark.executor.memory": "10g",
          "spark.sql.parquet.columnarReaderBatchSize": "32",
          "spark.driver.maxResultSize": "1t",
          "spark.task.maxDirectResultSize": "1t"
            }

Loading data
------------

And then you can load some data (it does however take more time to load then on SWAN):

.. code-block:: python

    df1 = DataQuery.builder(spark).entities().system('WINCCOA') \
        .keyValuesEq({'variable_name': 'MB.C16L2:U_HDS_3'}) \
        .timeWindow('2018-06-15 00:00:00.000', '2018-06-17 00:00:00.000') \
        .build()
