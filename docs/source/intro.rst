Introduction
============

``pyBT`` is a python module for the Beam Transfer Physics (BTP) section.


Motivation of the pyBT module
**********
This repo is a collection of codes for optics studies in the BTP section. This is a collaborative projects so feel free to add your functions !

ABT Quickstart
**********
`ABT Quickstart <https://gitlab.cern.ch/abt-optics-and-code-repository/misc/quickstart/-/blob/master/Quickstart.md?ref_type=heads>`_ contains some tutorials, tips and tricks for newcomers.
