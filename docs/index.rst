.. pyBT documentation master file, created by
   sphinx-quickstart on Wed Apr 24 13:40:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyBT's documentation!
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   source/intro
   source/pybt.tools
   source/pybt.myjson
   source/pybt.trackers
   source/run_nxcals_locally

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
