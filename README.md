# PyBT

Python package for ABT
Contains **or will** contain much

**Sphinx documetation** is built after every commit, on the master branch
It is available on a dedicated website [here](https://acc-py.web.cern.ch/gitlab/abt-optics-and-code-repository/simulation-codes/pybt), accessible inside CERN's netwrork or in pdf in the artifact [here](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/pybt/-/jobs/artifacts/master/raw/docs/pdf/pybt.pdf?job=build_docs_on_tag).

Can be installed inside CERN system using [CERN's package repo](http://acc-py-repo.cern.ch/)
```python
pip install --index-url http://acc-py-repo.cern.ch:8081/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch pybt
```
options for index-url and trusted-host may be ommited if CERN's repo is already configured in the pip configuration file, which is the case on the TN machines ACC-PY

Or without it, using the gitlab addreess
```python
pip install git+https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/pybt.git
```
