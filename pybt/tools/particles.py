"""
This module contains functions helping to generate and characterize particle distributions

"""
import numpy as np
import scipy.optimize as opt
from scipy.stats import truncexpon, truncnorm, uniform

def beam_distrib_norm(alpha, beta, eps, nparts, nsigma):
    """
    Generate a normally distributed particle distribution
    using the exponentially distributed actions.
    Allows cut distributions, including hollow ones.

    Parameters
    ----------
    alpha : scalar
        Twiss parameter
    beta : scalar
        Twiss parameter
    eps : scalar
        RMS emittance
    nparts : scalar
        Number of particles to be produced
    nsigma : length 2 array-like or scalar
       If scalar then we have the distribution only contains
       particles with 2J > nsigma**2*eps
       If array [n1,n2] then the distribution only contains
       oarticles with n1**2*eps > 2J > n2**2*eps
       The square is done so that the projection cut is in sigma

    Returns
    -------
    ndarray
        nparts*2 size array containing position and angle of each particle

    Examples
    --------


    Here we illustrate the function by plotting 2 distribution,
    from zero to 2 sigma on the left and from 1 to 3 sigma on the right

    .. plot::

        import numpy as np
        import matplotlib.pyplot as plt
        dist1 = pybt.tools.particles.beam_distrib_norm(0.5, 3, 3, 10000, 2)
        dist2 = pybt.tools.particles.beam_distrib_norm(0.5, 3, 3, 10000, [1,3])
        dist = np.array([(x,y) for x, y in dist1 if x<0] + [(x,y) for x, y in dist2 if x>0])
        plt.plot(dist[:,0], dist[:,1], 'bo', markersize=0.3)
        plt.xticks(np.arange(-2, 3.1, 1)*3)
        plt.yticks(np.arange(-3, 3.1, 1)*np.sqrt(1.25))
        plt.grid(True)
        plt.show()


    """
    if np.size(nsigma) == 2:
        nsigma = np.sort(np.array(nsigma))
        cs_inv = truncexpon(b=nsigma[1]**2/2-nsigma[0]**2/2,
                            scale=2*eps).rvs(nparts)+nsigma[0]**2*eps
    elif np.size(nsigma) == 1:
        cs_inv = truncexpon(b=nsigma**2/2, scale=2*eps).rvs(nparts)
    else:
        raise ValueError('sigma is either length 2 array or scalar', nsigma)

    angles = uniform(0, 2*np.pi).rvs(nparts)

    x = np.sqrt(cs_inv*beta)*np.cos(angles)
    xp = -np.sqrt(cs_inv/beta)*(np.sin(angles)+alpha*np.cos(angles))

    return np.column_stack([x, xp])

def beam_distrib_line(alpha, beta, eps, nparts, nsigma):
    """
    Generate particles regularly distributed along a single 
    ellipse at nsigma

    Parameters
    ----------
    alpha : scalar
        Twiss parameter
    beta : scalar
        Twiss parameter
    eps : scalar
        RMS emittance
    nparts : scalar
        Number of particles to be produced
    nsigma : scalar
       ellipse of particles with 2J = nsigma**2*eps

    Returns
    -------
    ndarray
        nparts*2 size array containing position and angle of each particle

    """
    cs_inv = np.ones(nparts)*nsigma**2*eps
    angles = np.linspace(0, 2*np.pi, nparts)
    x = np.sqrt(cs_inv*beta)*np.cos(angles)
    xp = -np.sqrt(cs_inv/beta)*(np.sin(angles)+alpha*np.cos(angles))

    return np.column_stack([x, xp])


def dpp_distrib_norm(dpp, nparts, nsigma):
    """
    Generate a 1D truncated Gaussian from a pdf with given standard deviation

    Parameters
    ----------
    dpp : scalar
        Standard deviation of the distribution
    nparts : scalar
        Number of particles to be produced
    nsigma : scalar
        sigma cut of the distribution, i.e. +- nsigma*dpp

    Returns
    -------
    ndarray
        nparts 1-D array containing the particles

    """ 
    return truncnorm.rvs(-nsigma, nsigma, scale=dpp, size=nparts)




def get_disp(x, dpp):
    r"""
    Returns the value of the dispersive term (D or D') extracted from a 
    distribution of particles using the correlation coeficient :

    .. math:: 
       D =\dfrac{cov(X,DPP)}{\sigma(DPP)^2} \quad \text{or} \quad
       D'=\dfrac{cov(PX,DPP)}{\sigma(DPP)^2} 

    Parameters
    ----------
    x : 1-D array or list
        Input position or angle distribution, X or PX
    dpp : 1-D array or list
        Momentum deviation distribution, DPP

    Returns
    -------
    scalar
        Dispersive term computed from the coordinates provided

    """
    cov = np.cov(x, dpp)
    D = cov[1,0]/cov[1,1]
    return D




def get_cov(alpha, beta, eps):
    r"""
    As one can characterize a distribution trought its first moments (covariance matrix),
    it can also be expressed as a fcuntion of twiss parameters. The covariance matrix, 
    also referred to as the sigma matrix is :

    .. math::
       \Sigma = \epsilon_{RMS} \begin{bmatrix} \beta & -\alpha \\  -\alpha & \gamma \end{bmatrix}
    
    Important
    -----
    The Twiss parameters returned are correct if and only if the dispersive component of the distribution is substracted first. *i.e.* the input needs to be :math:`x_{\beta} = x - D*dpp` ; :math:`xp_{\beta} = xp - D*dpp` ; **get_parms**:math:`(x_{\beta}\textrm{,} \, xp_{\beta})`. Dispersion and angular dispersion may be computed using **get_disp**.

    Parameters
    ----------
    alpha : scalar
        Twiss :math:`\alpha` parameter
    beta : scalar
        Twiss :math:`\beta` parameter
    eps : scalar
        Considered geometrical emittance

    Returns
    -------
    2-D ndarray
        Covariance, beam or sigma matrix associated with the provided Twiss paramters
        
    
    

    """                                   
    return eps*np.array([[beta, -alpha],[-alpha, (1+alpha**2)/beta]])


def get_parms(x, xp):
    r"""
    From provided distribution it returns the covariance matrix and associated Twiss parameters, opposite in function to  :func:`get_cov`

    Parameters
    ----------
    x : 1-D array or list
        Particle transverse position
    xp : 1-D array or list
        Complex conjugate of x, here ~angle

    Returns
    -------
    dic
        Dictionary of Twiss parameters *alpha* and *beta*, geometrical emittance *eps* and covariance 
        matrix *cov*
    
    
    """
    cov = np.cov(x, xp)
    eps = np.sqrt(cov[0,0]*cov[1,1]-cov[1,0]**2)
    alpha = -cov[1,0]/eps
    beta = cov[0,0]/eps
    return {'alpha':alpha, 'beta':beta, 'eps':eps, 'cov':cov}


def ellipse_frm_cov(cov, nsig=None, **kwargs):
    r"""
    Retrurns width, hight and angle of ellipse, from the covariance matrix

    taken from `https://stackoverflow.com/questions/12301071/multidimensional-confidence-intervals`

    Parameters
    ----------
    cov : 2-D array or list
        Covariance matrix

    msig : scalar
        To be checked and explained, and illustrated what this nsig is

    Returns
    -------
    list
        list of width, heigh and rotation of the ellipse

    """
    vals, vecs = np.linalg.eigh(cov)
    order = vals.argsort()[::-1]
    
    vals, vecs = vals[order], vecs[:,order]
    rotation = np.degrees(np.arctan2(*vecs[:,0][::-1]))
    width, height = 2 * nsig * np.sqrt(vals)

    return width, height, rotation
    
def _emittance(x , xp, alpha, beta, x0 = (0, 0)):
    '''
    Get emittance of a particle

    Parameters
    -----------

    x: float, numpy array
        h-cordinate of particles
    xp: float, numpy array
        v-coordinate of particles
    alpha: float
        Courant Snyder alpha parameter
    beta: float
        Courant Snyder beta parameter
    x0: 2-tuple
        Center of distribution

    Returns
    --------

    eps: float, numpy array
        emittance

    '''

    x_centered = x-x0[0]
    xp_centered = xp-x0[1]
    eps = (x_centered**2 + (alpha*x_centered + beta*xp_centered)**2)/beta
    return eps

def _frac_parts_in(x , xp, alpha, beta, eps0, x0 = (0, 0)):
    '''
    Get fraction of particles with emittance smaller than a given value

    Parameters
    -----------

    x: float, numpy array
        h-cordinate of particles
    xp: float, numpy array
        v-coordinate of particles
    alpha: float
        Courant Snyder alpha parameter
    beta: float
        Courant Snyder beta parameter
    eps0: float
        Threshold emittance
    x0: 2-tuple
        Center of distribution

    Returns
    --------

    frac_in: float
        fraction of particles inside the threshold

    '''

    eps = _emittance(x , xp, alpha, beta, x0 = x0)
    frac_in = (eps < eps0).sum()/len(eps)
    if frac_in < 1:
        return frac_in
    else:
        return 0



def _find_needed_emittance(x, xp, alpha, beta, eps0, x0 = (0, 0), fraction = 0.995, optimizer_kwargs = None):
    '''
    Emittance search to fit a certain fraction of the distribution

    Parameters
    -----------

    x: float, numpy array
        h-cordinate of particles
    xp: float, numpy array
        v-coordinate of particles
    alpha: float
        Courant Snyder alpha parameter
    beta: float
        Courant Snyder beta parameter
    eps0: float
        Initial guess for emittance
    x0: 2-tuple
        Center of distribution

    Returns
    --------

    frac_in: float
        fraction of particles inside the threshold


    '''

    default_optimizer_kwargs = {
        'method' : 'COBYLA',
        'options' : {'rhobeg': eps0},
        'x0' : eps0,
        'tol' : 1e-5/len(x)
     }


    if (optimizer_kwargs is None):
        optimizer_kwargs = default_optimizer_kwargs
    else: 
        default_optimizer_kwargs.update(optimizer_kwargs)
        optimizer_kwargs = default_optimizer_kwargs 


    func = lambda eps: abs(fraction - _frac_parts_in(x , xp, alpha, beta, eps, x0 = x0))
    res = opt.minimize(np.vectorize(func), **optimizer_kwargs)
    return res['x']


def find_best_ellipse(x, xp , alpha0, beta0, eps0, cost_fun = _find_needed_emittance, 
    cost_fun_kwargs = None, optimizer_kwargs = None):
    '''
    Find the best ellipse varying twiss parameters and ellipse center given a certain cost function

    Useful when the particle distribution is far from a 2-D Gaussian.
    In cases where the distribution is a highly assymetric non-gaussian,
    the rms estimation of Courant Snyder parameters produces a poor result.

    .. warning::
        The solution of this (highly flexible) optimization may not be unique!
        
        It is advisable to start with rms Courant-Snyder parameters as a initial guess and
        to visually check the result plotting the produced ellipse 

    .. warning:: 
        If distribution has huge outliers the routine could not work because the 
        centroid is taken as the initial guess for the ellipse center. It is recommended 
        to clip the distribution to say 4,5 standard deviations.

    Parameters
    -----------

    x: float, numpy array
        h-cordinate of particles
    xp: float, numpy array
        v-coordinate of particles
    alpha0: float
        Initial guess for Courant Snyder alpha parameter
    beta0: float
        Initial guess for Courant Snyder beta parameter
    eps0: float
        Initial guess for emittance 
    cost_fun: function
        Function to be minimized to quantify the 'goodness' of fit. 
        It should also output a value for the estimated emittance 

    Returns
    --------

    twiss_best: dict
        Optimized Courant Snyder parameters
    centre_best: tuple
        Optimized ellipse center

    '''

    x0 = (x.max() + x.min())/2 
    xp0 = (xp.max() + xp.min())/2

    default_optimizer_kwargs = {
        'method' : 'Nelder-Mead',
        'x0' : [x0, xp0, alpha0, beta0],
        }

    if cost_fun_kwargs is None:
        cost_fun_kwargs = {}

    if (optimizer_kwargs is None):
        optimizer_kwargs = default_optimizer_kwargs
    else: 
        default_optimizer_kwargs.update(optimizer_kwargs)
        optimizer_kwargs = default_optimizer_kwargs 


    res = opt.minimize(lambda s : cost_fun(x, xp, s[2], s[3], eps0, x0 = (s[0], s[1]), **cost_fun_kwargs),
     **optimizer_kwargs)

    twiss_best = {'alpha': res['x'][2], 'beta': res['x'][3], 'eps': res['fun']}
    centre_best = (res['x'][0], res['x'][1])
    return twiss_best, centre_best
