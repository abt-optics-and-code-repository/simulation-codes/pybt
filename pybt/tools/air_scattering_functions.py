import numpy as np
import pandas as pd

def manipulate_globals_and_return(madx, exn, eyn, p, beta, gamma, L, scattering=False):
    """
    Manipulates the global variables of a MADX instance and returns the new beta functions and emittance.
    If the scattering parameter is set to True, it also calculates the new emittance due to scattering.

    Parameters
    ----------
    madx : cpymad.madx.Madx
        An instance of the MADX class.
        
    exn : float
        The normalized horizontal emittance.
        
    eyn : float
        The normalized vertical emittance.
        
    p : float
        The momentum of the beam.
        
    beta : float
        The relativistic beta factor.
        
    gamma : float
        The relativistic gamma factor.
        
    L : float
        The length of the air region.
        
    scattering : bool, optional
        A flag indicating whether to calculate the new emittance due to scattering. Default is False.

    Returns
    -------
    float
        The new horizontal beta function.
        
    float
        The new vertical beta function.
        
    float
        The new horizontal alpha function.
        
    float
        The new vertical alpha function.
        
    float
        The new horizontal dispersion.
        
    float
        The new vertical dispersion.
        
    float
        The new horizontal momentum deviation.
        
    float
        The new vertical momentum deviation.
        
    float
        The new horizontal emittance.
        
    float
        The new vertical emittance.
    """

    madx.globals['betx0'] = 'savebeta_air->betx'
    madx.globals['bety0'] = 'savebeta_air->bety'
    madx.globals['alfx0'] = 'savebeta_air->alfx'
    madx.globals['alfy0'] = 'savebeta_air->alfy'
    madx.globals['dx0'] = 'savebeta_air->dx'
    madx.globals['dy0'] = 'savebeta_air->dy'
    madx.globals['dpx0'] = 'savebeta_air->dpx'
    madx.globals['dpy0'] = 'savebeta_air->dpy'

    if scattering==True:
        
        epsilon_0 = exn/(beta*gamma)
        epsilon_0_V = eyn/(beta*gamma)

        p_MeV = p*1000 # Beam total energy in MeV
        q = 1
        P = 1.01325 # Standard air pressure at sea level in Bar
        P_Torr = P*750.062 # Standard air pressure at sea level in Torr
        L_rad0 = 301 # For air. Table with radiation lengths: https://cds.cern.ch/record/941314/files/p245.pdf
        L_rad = L_rad0/(P_Torr/760)

        theta_rms = (13.6/p_MeV*beta)*q*np.sqrt(L/L_rad)

        # Horizontal
        gamma0 = (1 + madx.globals['alfx0']**2) / madx.globals['betx0']
        Delta_epsilon = (1/2)*theta_rms**2*(madx.globals['betx0'] + L*madx.globals['alfx0'] + (L**2/3)*gamma0)
        epsilon_1 = epsilon_0 + Delta_epsilon
        alfx0_1 = (epsilon_0*madx.globals['alfx0'] - (L/2)*theta_rms**2 ) / (epsilon_0 + Delta_epsilon)
        betx0_1 = (epsilon_0*madx.globals['betx0'] + (L**2/3)*theta_rms**2 ) / (epsilon_0 + Delta_epsilon)

        # Vertical
        gamma0_V = (1 + madx.globals['alfy0']**2) / madx.globals['bety0']
        Delta_epsilon_V = (1/2)*theta_rms**2*(madx.globals['bety0'] + L*madx.globals['alfy0'] + (L**2/3)*gamma0_V)
        epsilon_1_V = epsilon_0_V + Delta_epsilon_V
        alfy0_1 = (epsilon_0_V*madx.globals['alfy0'] - (L/2)*theta_rms**2 ) / (epsilon_0_V + Delta_epsilon_V)
        bety0_1 = (epsilon_0_V*madx.globals['bety0'] + (L**2/3)*theta_rms**2 ) / (epsilon_0_V + Delta_epsilon_V)

        # Return
        exn = epsilon_1*beta*gamma
        eyn = epsilon_1_V*beta*gamma
        betx0 = betx0_1
        alfx0 = alfx0_1
        bety0 = bety0_1
        alfy0 = alfy0_1
        
    else:
        betx0 = madx.globals['betx0']
        alfx0 = madx.globals['alfx0']
        bety0 = madx.globals['bety0']
        alfy0 = madx.globals['alfy0']
    
    dx0 = madx.globals['dx0']
    dy0 = madx.globals['dy0']
    dpx0 = madx.globals['dpx0']
    dpy0 = madx.globals['dpy0']

    return betx0, bety0, alfx0, alfy0, dx0, dy0, dpx0, dpy0, exn, eyn

def add_air_region(madx, title, sequence, air_start_pos, air_stop_pos, steps):
    """
    Adds an air region to the MADX sequence by defining and installing start, end, and intermediate markers.
    
    This function first calculates the positions of intermediate markers, then adds and installs markers 
    for the start and end of the air region as well as the intermediate markers.

    Parameters
    ----------
    madx : cpymad.madx.Madx
        An instance of the MADX class.

    title : str
        The title to be used in the inner markers' names.
        
    sequence : str
        The name of the sequence to which the markers are added.
        
    air_start_pos : float
        The starting position of the air region in meters.
        
    air_stop_pos : float
        The stopping position of the air region in meters.
        
    steps : float
        The distance in meters between two consecutive inner markers.

    Returns
    -------
    None
    """

    # Calculate the positions of the inner markers
    inner_marker_array = np.arange(air_start_pos+steps, air_stop_pos, steps)

    # Add the markers for the start and end of the air region
    madx.input(f'''
    AIR_START : MARKER;
    AIR_END : MARKER;
    ''')           

    # Install the markers for the start and end of the air region
    madx.input(f'''
    SEQEDIT, SEQUENCE={sequence};
    INSTALL, ELEMENT=AIR_START, CLASS=AIR_START, AT={air_start_pos};
    INSTALL, ELEMENT=AIR_END, CLASS=AIR_END, AT={air_stop_pos};
    ENDEDIT;
    ''')

    # Add the inner markers
    madx.input(f"SEQEDIT, SEQUENCE = {sequence};")
    # Create a for loop for the inner markers:
    for count, pos in enumerate(inner_marker_array):
        madx.input(f"INSTALL, ELEMENT=INNER_MARKER{str(count)}_{title}, CLASS=MARKER, AT={pos};")
        # print(f"INNER_MARKER{str(count)}")
    madx.input("ENDEDIT;")

    return

def add_hidden_marker(madx, sequence, start, stop, steps):
    """
    Adds hidden markers to a given MADX sequence at specified intervals for detailed beam evolution tracking.

    This function iteratively defines and installs hidden markers at regular steps within a specified range in the sequence.

    Parameters
    ----------
    madx : cpymad.madx.Madx
        An instance of the MADX class.
        
    sequence : str
        The name of the sequence to which the markers are added.
        
    start : float
        The starting position in meters for the first marker in the sequence.
        
    stop : float
        The stopping position in meters for the last marker in the sequence.
        
    steps : float
        The distance in meters between two consecutive hidden markers.

    Returns
    -------
    None
    """

    # Add marker at the end of the line to see the evolution of the beam with fine resolution
    madx.input(f"SEQEDIT, SEQUENCE = {sequence};")
    for count, pos in enumerate(np.arange(start, stop, steps)):
        madx.input(f"INSTALL, ELEMENT=HIDDEN_MARKER{str(count)}, CLASS=MARKER, AT={pos};")
    madx.input("ENDEDIT;")

    return

def process_scattering(madx, exn, eyn, beta, gamma, sequence, end, p, twiss_initial, ex_initial, ey_initial):
    """
    Processes the scattering in a given MADX sequence.

    The function calculates the effects of scattering through the sequence by iterating over all air regions 
    and updating the beam and twiss parameters accordingly. 

    Parameters
    ----------
    madx : cpymad.madx.Madx
        An instance of the MADX class.
    exn : float
        Normalized horizontal emittance.
    eyn : float
        Normalized vertical emittance.
    beta : float
        Relativistic beta factor.
    gamma : float
        Relativistic gamma factor.
    sequence : str
        The name of the sequence to process.
    end : str
        The name of the end element of the sequence.
    p : float
        Momentum of the particles in GeV/c.
    twiss_initial : pandas.DataFrame
        The initial twiss parameters of the sequence.
    ex_initial : float
        The initial horizontal emittance.
    ey_initial : float
        The initial vertical emittance.

    Returns
    -------
    tuple
        A tuple containing two dataframes: the initial and the scattered twiss parameters of the sequence.
    """

    air_start_elements = [name for name in twiss_initial.name if "air_start" in name]
    # print(f"There are {len(air_start_elements)} air regions in the sequence: {air_start_elements}")

    air_start_numbers = [int(name.split(':')[1]) for name in air_start_elements]
    # print(f"The air regions are numbered: {air_start_numbers}")

    inner_elements = [name for name in twiss_initial.name if "inner" in name]

    def update_beam(madx, exn, eyn, beta, gamma, ex_list, ey_list):
        madx.command.beam(ex=exn/(beta*gamma), ey=eyn/(beta*gamma))
        ex_list.append(madx.sequence[sequence].beam.ex)
        ey_list.append(madx.sequence[sequence].beam.ey)
        return madx, ex_list, ey_list

    def append_twiss_to_list(madx, sequence, twiss_list, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0):
        madx.use(sequence=sequence)
        twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
        twiss_list.append(twiss)
        return twiss_list

    twiss_list = []
    ex_list = []
    ey_list = []

    betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn, eyn = manipulate_globals_and_return(madx, exn, eyn, p, beta, gamma, scattering=False, L=0)

    madx.input(f"SEQEDIT, SEQUENCE = {sequence};")
    madx.input(f"EXTRACT, SEQUENCE = {sequence}, FROM=AIR_START, TO={end}, NEWNAME=seq_air_start_to_end;")
    madx.input("ENDEDIT;")

    previous_seq_name = "seq_air_start_to_end"

    # Iterating over all air regions
    for air_region_number in air_start_numbers:
        # print(f"Entering air region {air_region_number}")

        if air_region_number == 1:
            air_start_pos = madx.sequence[sequence].elements["air_start"].position

        else:
            air_start_pos = madx.sequence[sequence].elements[f"air_start[{str(air_region_number)}]"].position

        # print(air_start_pos)

        inner_marker_list = [name for name in inner_elements if name.endswith(f"{air_region_number}:1")]
        i = 0
        for i in range(len(inner_marker_list)):

            if i == 0:
                step = madx.sequence[sequence].elements[inner_marker_list[i][:-2]].position - air_start_pos
            else:
                step = madx.sequence[sequence].elements[inner_marker_list[i][:-2]].position - madx.sequence[sequence].elements[inner_marker_list[i-1][:-2]].position

            madx, ex_list, ey_list = update_beam(madx, exn, eyn, beta, gamma, ex_list, ey_list)

            # print(f"SAVEBETA at INNER_MARKER{str(i)}_{str(air_region_number)}")
            madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = INNER_MARKER{str(i)}_{str(air_region_number)}, SEQUENCE={previous_seq_name};")
            twiss_list = append_twiss_to_list(madx,previous_seq_name, twiss_list, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0)
            betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn, eyn = manipulate_globals_and_return(madx, exn, eyn, p, beta, gamma, scattering=True, L=step)

            # print(f"Extracting from INNER_MARKER{str(i)}_{str(air_region_number)} to {end}")
            madx.input(f"SEQEDIT, SEQUENCE = {previous_seq_name};")
            madx.input(f"EXTRACT, SEQUENCE= {previous_seq_name}, FROM=INNER_MARKER{str(i)}_{str(air_region_number)}, TO={end}, NEWNAME=seq_inner_marker{str(i)}_to_end;")
            madx.input("ENDEDIT;")

            previous_seq_name = f"seq_inner_marker{str(i)}_to_end"

            print(i)
        
        madx, ex_list, ey_list = update_beam(madx, exn, eyn, beta, gamma, ex_list, ey_list)

        #### Air_END ####
        # Final step calculation
        if air_region_number == 1:
            air_end_pos = madx.sequence[sequence].elements["air_end"].position
            step = air_end_pos - madx.sequence[sequence].elements[inner_marker_list[i][:-2]].position
            print("ok1")
            print(step, i)
        else:
            air_end_pos = madx.sequence[sequence].elements[f"air_end[{str(air_region_number)}]"].position
            step = air_end_pos - madx.sequence[sequence].elements[inner_marker_list[i][:-2]].position
            print("ok2")
            print(step, i)

        # print(f"SAVEBETA at AIR_END")
        if air_region_number == 0:
            madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = AIR_END, SEQUENCE={previous_seq_name};")
        else:
            madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = AIR_END[{str(air_region_number)}], SEQUENCE={previous_seq_name};")

        twiss_list = append_twiss_to_list(madx,previous_seq_name, twiss_list, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0)
        betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, exn, eyn = manipulate_globals_and_return(madx, exn, eyn, p, beta, gamma, scattering=True, L=step)
        
        madx.input(f"SEQEDIT, SEQUENCE = {previous_seq_name};")
        if air_region_number == 1:
            # print(f"Extracting from AIR_END to {end}")
            madx.input(f"EXTRACT, SEQUENCE= {previous_seq_name}, FROM=AIR_END, TO={end}, NEWNAME=seq_air_end_to_end;")
        else:
            # print(f"Extracting from AIR_END[{str(air_region_number)}] to {end}")
            madx.input(f"EXTRACT, SEQUENCE= {previous_seq_name}, FROM=AIR_END, TO={end}, NEWNAME=seq_air_end{str(air_region_number)}_to_end;")
        madx.input("ENDEDIT;")

        
        if air_region_number == 1:
            previous_seq_name = f"seq_air_end_to_end"
        else:
            previous_seq_name = f"seq_air_end{str(air_region_number)}_to_end"

        twiss_list = append_twiss_to_list(madx,previous_seq_name, twiss_list, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0)
        madx, ex_list, ey_list = update_beam(madx, exn, eyn, beta, gamma, ex_list, ey_list)


    # Shift the s position so that is is coherent.
    for i in range(len(twiss_list)):
        twiss_list[i].s = twiss_list[i].s + twiss_initial.loc[twiss_list[i].iloc[1].name].s


    # We add the emittances to each twiss table
    twiss_initial['ex'] = ex_initial

    for i in range(len(twiss_list)):
        twiss_list[i]['ex'] = ex_list[i]

    twiss_initial['ey'] = ey_initial

    for i in range(len(twiss_list)):
        twiss_list[i]['ey'] = ey_list[i]

    # We remove the drifts as they are duplicated names
    def remove_drift_rows(df):
        df = df.loc[~df.index.str.startswith('drift')]
        df = df.loc[~df.index.str.startswith('#s')]
        return df

    twiss_list = [remove_drift_rows(df) for df in twiss_list]

    # We combine all the twiss tables into one
    def add_missing_rows(df1, df2):
        # Find rows in df1 with lower 's' than the minimum 's' in df2
        missing_rows = df1[df1['s'] < df2['s'].min()]
        
        # Append these rows to df2 and sort by 's'
        df2_updated = pd.concat([df2, missing_rows]).sort_values(by='s')

        return df2_updated

    # Loop through each element in the list
    for i in range(1, len(twiss_list)):
        twiss_list[i] = add_missing_rows(twiss_list[i-1], twiss_list[i])

    # Finaly, we add the initial twiss table to the beginning
    twiss_scattered = add_missing_rows(twiss_initial, twiss_list[-1])

    return twiss_initial, twiss_scattered