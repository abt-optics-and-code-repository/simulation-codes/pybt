import pybt
import matplotlib.pyplot as plt
from pathlib import Path

header, twiss = pybt.read_twiss_file(str(Path(pybt.__file__).parent.absolute() / 'tests/twiss.tfs'))
fig = plt.figure(figsize=(15,7))
pybt.tools.plotters.draw_optics(header, twiss, fig, ['quadrupole'])
fig.axes[3].set_ylim(fig.axes[4].get_ylim())
