import matplotlib.pyplot as plt
import numpy as np
import pybt
from pathlib import Path

header, twiss = pybt.read_twiss_file(str(Path(pybt.__file__).parent.absolute() / 'tests/twiss.tfs'))
fig = plt.figure(figsize=(15,7))
pybt.tools.plotters.draw_optics(header, twiss, fig, ['quadrupole'])

particlesx = pybt.tools.particles.beam_distrib_norm(
    *twiss.iloc[0][['alfx', 'betx']], header['ex'], 10, [4-1e-6,4+1e-6])

particlesy = pybt.tools.particles.beam_distrib_norm(
    *twiss.iloc[0][['alfy', 'bety']], header['ey'], 10, [4-1e-6,4+1e-6])

_ = fig.axes[3].autoscale(False), fig.axes[4].autoscale(False)

for particle in np.hstack((particlesx, particlesy, np.zeros((10,2)))):
    fig.axes[3].plot(twiss['s'],
                    pybt.tools.twiss_track.track_with_re(twiss, 'x', particle),
                    'r-', alpha=0.2)
    fig.axes[4].plot(twiss['s'],
                    pybt.tools.twiss_track.track_with_re(twiss, 'y', particle),
                    'b-', alpha=0.2)
