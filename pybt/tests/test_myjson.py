"""
High-level tests for the myjson module.

"""
from pybt import myjson
from pybt.myjson.encoder import myJSONEncoder, myJSONDecoder
import json
import pickle
import datetime
import numpy as np
from pathlib import Path


def test_dump_load():
    log = pickle.load(open(Path(__file__).parent.absolute() / 'log.p', 'rb'))
    test = checkDumpLoadJSON(log)
    compareDumpLoadJSON(log, test)

def checkDumpLoadJSON(data):
    return json.loads(json.dumps(data, cls=myJSONEncoder), cls=myJSONDecoder)

def compareDumpLoadJSON(data,data2):
    compareOK = True
    iter_data2 = traverse(data2)
    for k, v in traverse(data):
        k2, v2 = next(iter_data2)
        if isinstance(v, (np.ndarray,)):
            if not (v == v2).all():
                print('Warning, check difference detected!')
                print(f"{k} : original = {v}, dumped/loaded = {v2} ")
                compareOK = False
        else:
            if not v == v2:
                print('Warning, check difference detected!')
                print(f"{k} : original = {v}, dumped/loaded = {v2} ")
                compareOK = False
    if compareOK:
        print('Comparison check complete: OK!')
    else:
        print('Comparison check failed!')

def checkWriteTimePickle(data):
    t1 = datetime.datetime.now()
    fileObj = open(Path(__file__).parent.absolute() / 'test.p', "wb")
    pickle.dump(data, fileObj)
    fileObj.close()
    t2 = datetime.datetime.now()
    return t2 - t1

def checkWriteTimeJSON(data):
    t1 = datetime.datetime.now()
    with open(Path(__file__).parent.absolute() / 'test.json', 'w') as outfile:
        outfile.write(json.dumps(data, cls=myJSONEncoder))
    t2 = datetime.datetime.now()
    return t2 - t1

def test_compareWriteTimeJSONvsPickle():
    log = pickle.load(open(Path(__file__).parent.absolute() / 'log.p', 'rb'))
    JSON = checkWriteTimeJSON(log)
    Pickle = checkWriteTimePickle(log)
    print('Write time pickle = ', Pickle)
    print('Write time JSON = ', JSON)
    return JSON, Pickle

# Create generator for dict
def traverse(value, key=None):
    if isinstance(value, dict):
        for k, v in value.items():
            yield from traverse(v, k)
    else:
        yield key, value

def print_data(d):
    for k, v in traverse(d):
        print(f"{k} : {type(v)}")
