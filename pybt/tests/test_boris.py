import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pybt
from pybt.trackers.boris import track_to_condition


fig, ax = plt.subplots(1, 1, figsize=(7, 7))

a, b, c = 2, -1, 0.5
track_is_inside = lambda x, p, t : -b*x[1]>a*x[0]+c

text = ''
for y0 in np.linspace(2.5, 10, 4):
    xs, ps, ts, _ = track_to_condition(track_is_inside, [0, y0, 0], 
                                       np.array([10*pybt.trackers.boris.c, 0, 0]), 0, 0.938e9, 1, 
                                       1/3*1e-9, 
                                       return_full_track=True, func_B=lambda x, time : np.array([0, 0, 1.]) )
    df = pd.DataFrame()
    xs = np.array(xs)
    df['x'], df['y'], df['z'] = xs[:,0], xs[:, 1], xs[:, 2]

    ax.set_xlabel('x (m)')
    ax.set_ylabel('y (m)')
    ax.plot(df['x'], df['y'], 'rx', markersize=3)
    text += 'trajectory starting at y={:5.1f} m, distance of the last point to the blue line is {:10.2e} m \n'.format\
          (y0,np.abs(a*xs[-1,0]+b*xs[-1,1]+c)/np.sqrt(a**2+b)**2)

X = np.array([0, 5])
ax.plot(X, 2*X+0.5, 'b-')
ax.set_title(text)
