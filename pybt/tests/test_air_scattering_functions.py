import unittest
from pybt.tools.air_scattering_functions import process_scattering, add_air_region
from cpymad.madx import Madx

class TestProcessScattering(unittest.TestCase):
    def setUp(self):
        # self.madx = Madx(stdout=False, stderr=False)
        with open('tempfile', 'w') as f:
            self.madx = Madx(stdout=f,stderr=f)
            self.madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

        self.exn = 0.5
        self.eyn = 0.5
        self.beta = 0.999
        self.gamma = 1
        self.sequence = "simple_seq"
        self.end = "end"
        self.p = 24.0
        self.ex = self.exn/(self.beta*self.gamma)
        self.ey = self.eyn/(self.beta*self.gamma)
        self.ex_initial = self.ex
        self.ey_initial = self.ey
        self.betx0 = 1
        self.bety0 = 1
        self.alfx0 = 0
        self.alfy0 = 0
        self.Dx0 = 0
        self.Dy0 = 0
        self.Dpx0 = 0
        self.Dpy0 = 0

    def test_process_scattering(self):
        
        self.madx.input('''

        QF : QUADRUPOLE, L = 2, APERTYPE=CIRCLE, APERTURE={0.025};
        QD : QUADRUPOLE, L = 2, APERTYPE=CIRCLE, APERTURE={0.025};

        QF1 : QF, K1 := kQF1;
        QD2 : QD, K1 := kQD2;
        QF3 : QF, K1 := kQF3;
        QD4 : QD, K1 := kQD4;

        kQF1 = 0.32730047;
        kQD2 = -0.36102915;
        kQF3 = 0.32789126;
        kQD4 = -0.1991137;

        simple_seq: SEQUENCE, refer = exit, l = 100;
        QF1 : QF1, AT=2;
        QD2 : QD2, AT=5;
        QF3 : QF3, AT=8;
        QD4 : QD4, AT=11;
        END : MARKER, AT=100;
        ENDSEQUENCE;
        ''')

        # Add the air regions
        add_air_region(self.madx, "1", self.sequence, 20, 30, 1)
                
        self.madx.command.beam(particle='PROTON',pc=self.p,ex=self.ex,ey=self.ey)
        self.madx.input('BRHO      := BEAM->PC * 3.3356;')
        self.madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
        self.madx.input(f"SAVEBETA, LABEL=savebeta_air, PlACE = AIR_START, SEQUENCE={self.sequence};")
        self.madx.use(sequence=self.sequence)
        self.twiss_initial = self.madx.twiss(betx=self.betx0, bety=self.bety0, alfx=self.alfx0, alfy=self.alfy0, Dx=self.Dx0, Dy=self.Dy0, Dpx=self.Dpx0, Dpy=self.Dpy0).dframe()
        
        # Run the function
        process_scattering(self.madx, self.exn, self.eyn, self.beta, self.gamma, self.sequence, self.end, self.p, self.twiss_initial, self.ex_initial, self.ey_initial)


if __name__ == '__main__':
    unittest.main()