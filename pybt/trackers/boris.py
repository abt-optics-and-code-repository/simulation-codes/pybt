"""
This module contains the boris tracker and some helper functions
"""
import numpy as np
from scipy.optimize import minimize_scalar


e = 1.602176634e-19
c = 299792458.0

_cross3 = lambda a, b: np.array([
    a[1]*b[2] - a[2]*b[1],
    a[2]*b[0] - a[0]*b[2],
    a[0]*b[1] - a[1]*b[0]
])

_sum_sq = lambda a:  a[0]**2 + a[1]**2 + a[2]**2


def boris_stepper(position_k, momentum_k, time, mass, n_charges, time_step,
                  func_B=None, func_E=None, return_field=False):
    r"""
    This implementation of the Boris algorith tracks charged particles in E&M fields using the time as independant and was derived from [1]_ and [2]_.
    Motion of charged particle is governed by the Lorentz force, which we can express as

    .. math::         
       \frac{d\mathbf{u}}{dt} = \frac{q}{m} \left(\mathbf{E} + \mathbf{v} \times \mathbf{B} \right) = \frac{q}{m} \left(\mathbf{E} + \frac{\mathbf{u}}{\gamma} \times \mathbf{B} \right)  

    .. |br| raw:: html

        <br>
    
    With: |br|

    * :math:`\mathbf{u}=\frac{\mathbf{p}}{m}` with :math:`\mathbf{p}` the relativistic momentum and :math:`m` the rest mass 
    * :math:`\mathbf{x}` be the particle position in real space
    * :math:`\mathbf{v}` be the speed of the particle
    * :math:`q` be the charge of the particle
    * the particle velocity :math:`\mathbf{v} = \frac{d\mathbf{x}}{dt}= \frac{\mathbf{u}}{\gamma}`
    * :math:`\gamma` be the lorentz factor
    * :math:`\mathbf{E}` and :math:`\mathbf{B}` respectively be the electric and magnetic field

    The idea is to discretize the evolution of the position and momentum. It comes that the change in position over a discrete time step :math:`\Delta t` between the steps :math:`k` and :math:`k+1`

    .. math::
       \frac{\mathbf{x}_{k+1} - \mathbf{x}_k}{\Delta t} = \frac{\mathbf{u}}{\gamma}
       
    And the change in momentum from the lorentz force:

    .. math::
       \frac{\mathbf{u}_{k+1} - \mathbf{u}_k}{\Delta t} = \frac{q}{m} \left(\mathbf{E}_k + \bar{\mathbf{v}} \times \mathbf{B} \right) 

    Where the speed is an average over the step defineed as:

    .. math::
       \bar{\mathbf{v}} = \frac{\mathbf{u}_{k+1}-\mathbf{u}_k}{2 \gamma_{k+1/2}}

    The idea of the **Boris** algorithm is to push the particle by half a step using the momentum at step :math:`k` to find :math:`\mathbf{x}_{k+1/2}` and then apply the lorentz force.           

    .. math::
       \mathbf{x}_{k+1/2} = \mathbf{x}_k + \frac{\mathbf{u}_k}{\gamma_k} \frac{\Delta t}{2} 

    Now that we have established the discretization and the position at the middle of the step we can apply the 3 fundamental steps of the Boris methods:

    **1.** The first half of the electric field is applied to the momentum

    .. math::
       \mathbf{u}^- = \mathbf{u}_k + \frac{q}{m} \frac{\Delta t}{2} \mathbf{E}(\mathbf{x}_{k+1/2})

    **2.** The mometum :math:`\mathbf{u}^-` is rotated around the magntic field

    .. math::
       \mathbf{u}^+ = \mathbf{u}^- + \left(\mathbf{u}^- + \left(\mathbf{u}^- \times \mathbf{t} \right) \right) \times \mathbf{s} 

    where: |br|

    * :math:`\mathbf{t} = \mathbf{B}(\mathbf{x}_{k+1/2}) \frac{q}{m \gamma^-} \frac{\Delta t}{2}`
    * :math:`\gamma^- = \sqrt{1-(|\mathbf{u^-}|/c)^2} = \gamma^+ = \gamma_{k+1/2}` since there is only a rotation between :math:`\mathbf{u}^-` and :math:`\mathbf{u}^+`
    * :math:`\mathbf{s} = \frac{2 \mathbf{t}}{1 + |\mathbf{t}|^2}`

    **3.** Then the second half of the electric field 

    .. math::
       \mathbf{u}_{k+1} = \mathbf{u}^+ \frac{q}{m} \frac{\Delta t}{2} \mathbf{E}(\mathbf{x}_{k+1/2}) 

    Finally we push the particle by another half step using the momentum :math:`\mathbf{u}_{k+1}`

    .. math::
       \mathbf{x}_{k+1} = \mathbf{x}_{k+1/2} + \frac{\mathbf{u}_{k+1}}{\gamma_{k+1}} \frac{\Delta t}{2}
           
    Parameters
    ----------
    position_k : 3-long 1D-array
                 Initial particle position in :math:`m`
    momentum_k : 3-long 1D-array
                 Initial particle momentum in :math:`eV/c`
    time : float
           initial time in :math:`s`
    mass : float
           particle mass in :math:`eV/c^2`
    n_charges : float or int
                number of elementary charges 
    time_step : float
                time step in :math:`s`
    func_B : callable, optional
             The function returning the magnetic field in Tesla |br|

             It must be of the form :math`f(t, \mathbf{x}` with :math:`t` the time, a float and :math:`\mathbf{x}` the position, a 3-long list or array
    func_E : callable, optional
             The function returning the electric field in V/m |br|

             It must be of the form :math`f(t, \mathbf{x}` with :math:`t` the time, a float and :math:`\mathbf{x}` the position, a 3-long list or array
    return_field : optional, boolean
                   if True, returns also the fields and position at the middle of the step

    Returns
    -------

    position_k1 : 3-long 1D-array
                  particle position after the step
    momentum_k1 : 3-long 1D-array
                  particle momentum after the step
    time : float
           particle time after the step

    position_k12 : 3-long 1D-array
                   particle position in the middle of the step

    B12 : 3-long 1D-array
          Magnetic field in the middle of the step
    
    E12 : 3-long 1D-array
          Electric field in the middle of the step

    Examples
    --------

    Example of the tracking of a single proton in a dipole magnetic field perpendicular to the velocity that shall lead to a perfectly circular trajectory.

    .. plot::

        import matplotlib.pyplot as plt
        import numpy as np
        import pandas as pd
        import time
        
        p = np.array([10*pybt.trackers.boris.c, 0, 0])
        x = np.array([0., 10, 0])
        t, sys_t = 0., 0.
        
        total_tracking_time = 2*np.pi*10/pybt.trackers.boris.c*10
        func_B = lambda x, time : np.array([0, 0, 1.]) #pure and constant vertical field
        time_step = 1/3*1e-9
        
        xs, ps, ts = [], [], []
        
        sys_t = time.time()
        while t < total_tracking_time:
            x, p, t, _ = pybt.trackers.boris.boris_stepper(x, p, t, 0.938e9, 1, time_step, func_B=func_B)
            xs.append(x)
            ps.append(p)
            ts.append(t)
        sys_t = time.time() - sys_t       
        
        df = pd.DataFrame()
        xs = np.array(xs)
        df['x'], df['y'], df['z'] = xs[:,0], xs[:, 1], xs[:, 2]
        ps = np.array(ps)
        df['px'], df['py'], df['pz'] = ps[:,0], ps[:, 1], ps[:, 2]
        df['t'] = ts
        
        fig, axs = plt.subplots(1, 2, figsize=(15, 7))
        
        axs[0].set_title('Trajectory')
        axs[0].set_xlabel('x (m)')
        axs[0].set_ylabel('y (m)')
        axs[0].plot(df['x'], df['y'], 'r.', markersize=0.2)
        
        axs[1].set_title('Radius evolution')
        axs[1].set_xlabel('step x1000')
        axs[1].set_ylabel('Radius -10m')
        axs[1].plot(np.array(df.index)*1e-3, np.sqrt(df['x']**2+df['y']**2)-10, 'r.', markersize=0.2)
        fig.suptitle('Tracking of {:} step within {:5.1f} s or {:5.3f} ms/step'.format(len(xs), sys_t, sys_t/len(xs)*1e3))


    References
    ----------
    .. [1] `Why is Boris algorithm so good?`, H. Qin *et al.* Phys. Plasmas 20, 084503 (2013); `doi:10.1063/1.4818428 <https://doi.org/10.1063/1.4818428>`_ 
    .. [2] `A Comprehensive Comparison of Relativistic Particle Integrators` ,  B. Ripperda *et al* ApJS 235 21 (2018); `doi:10.3847/1538-4365/aab114 <https://doi.org/10.3847/1538-4365/aab114>`_
    """
    #     # p is in eV/c, position in m, mass in eV/c2
    # Magnetic and electric field respectively in Tesla and V/m

    # normalised momentum, in units of c !
    u_k = momentum_k/mass 
    gamma_k = np.sqrt(1+_sum_sq(u_k))

    
    if func_B is None and func_E is None:
        # if no lorentz force then simple push of the particle
        u_k1 = u_k
        position_k1 = position + u_k/gamma_k*time_step*c # since u/gamma is beta
    else:
        # first half step, using momentum at k
        position_k12 = position_k + u_k/gamma_k*time_step/2*c # first half step

        # first half of the electric field
        if func_E is not None: 
            E12 = func_E(position_k12, time)
            u_m = u_k +n_charges/mass*time_step/2*E12*c
            gamma_m = np.sqrt(1+_sum_sq(u_m)) 
        else:
            u_m = u_k
            gamma_m = gamma_k
            E12 = np.array([0., 0., 0.])

        # rotation step
        if func_B is not None: 
            # lorentz factor after the electric field kick
            B12 = func_B(position_k12, time)
            t = n_charges/(mass*gamma_m)*time_step/2*B12*c**2
            s = 2*t/(1+_sum_sq(t))
            u_p = u_m + _cross3(u_m + _cross3(u_m, t), s)
        else:
            u_p = u_m
            B12 = np.array([0., 0., 0.])

        # second half of the electric field, 
        if func_E is not None:
            u_k1 = u_p + n_charges/mass*time_step/2*E12*c
            gamma_k1 = np.sqrt(1+_sum_sq(u_k1))
        else:
            u_k1 = u_p
            gamma_k1 = gamma_k

        # second half step, using momentum at k+1
        position_k1 = position_k12 + u_k1/gamma_k1*time_step/2*c 
        momentum_k1 = u_k1*mass
        
    if return_field:
        return position_k1, u_k1*mass, time+time_step, (position_k12, B12, E12)
    else:
        return position_k1, u_k1*mass, time+time_step, ()

def _last_step_maximizer(track_is_inside, x, p, t, mass, n_charges, last_time_step, **kwargs):
    r"""
    Private function to maximize the length of the last timestep while track_is_inside
    """
    x_last, p_last, t_last, _ = boris_stepper(x, p, t, mass, n_charges, last_time_step, **kwargs)
    
    val = 1/last_time_step if track_is_inside(x_last, p_last, t_last) else last_time_step*10
    return val


def track_to_condition(track_is_inside, x, p, t, mass, n_charges, time_step,
                       return_full_track=False, xatol=1e-18, **kwargs):
    r"""
    This function wraps around the boris_stepper to track while a function `track_is_inside` is True, and takes care of the last step by maximising its length while staying as near as the `track_is_inside` edge as possible

    Parameters
    ----------
    track_is_inside : callable
                      Returns True when tracking is allowed and False otherwise, also triggers the last step and the end of the tracking
                      `track_is_inside(x, p, t) -> Bool`
    x : 3-long 1D-array
        Initial particle position in :math:`m`
    p : 3-long 1D-array
        Initial particle momentum in :math:`eV/c`
    t : float
        initial time in :math:`s`
    mass : float
           particle mass in :math:`eV/c^2`
    n_charges : float or int
                number of elementary charges 
    time_step : float
                time step in :math:`s`
    kwargs : 
             Keyword arguments passed to `boris_stepper`

    Examples
    --------
    
    Example of a tracking that end on a line of equation :math:`ax + bx + c`

    .. plot::

        import matplotlib.pyplot as plt
        import numpy as np
        import pandas as pd
        from pybt.trackers.boris import track_to_condition


        fig, ax = plt.subplots(1, 1, figsize=(7, 7))

        a, b, c = 2, -1, 0.5
        track_is_inside = lambda x, p, t : -b*x[1]>a*x[0]+c

        text = ''
        for y0 in np.linspace(2.5, 10, 4):
            xs, ps, ts, _ = track_to_condition(track_is_inside, [0, y0, 0], 
                                               np.array([10*pybt.trackers.boris.c, 0, 0]), 0, 0.938e9, 1, 
                                               1/3*1e-9, 
                                               return_full_track=True, func_B=lambda x, time : np.array([0, 0, 1.]) )
            df = pd.DataFrame()
            xs = np.array(xs)
            df['x'], df['y'], df['z'] = xs[:,0], xs[:, 1], xs[:, 2]

            ax.set_xlabel('x (m)')
            ax.set_ylabel('y (m)')
            ax.plot(df['x'], df['y'], 'rx', markersize=3)
            text += 'trajectory starting at y={:5.1f} m, distance of the last point to the blue line is {:10.2e} m \n'.format\
                  (y0,np.abs(a*xs[-1,0]+b*xs[-1,1]+c)/np.sqrt(a**2+b)**2)

        X = np.array([0, 5])
        ax.plot(X, 2*X+0.5, 'b-')
        ax.set_title(text)

    """
    field = ()
    xs, ps, ts, fields = [], [], [], []
    while track_is_inside(x, p, t):
        if return_full_track or len(ts)==0:
            xs.append(x)
            ps.append(p)
            ts.append(t)
            fields.append(field)
        else:
            xs[-1] = x
            ps[-1] = p
            ts[-1] = t
            fields[-1] = field
        x, p, t, field = boris_stepper(x, p, t, mass, n_charges, time_step, **kwargs)
    else:
        
        res = minimize_scalar(lambda last_time_step: _last_step_maximizer
                              (track_is_inside, xs[-1], ps[-1], ts[-1], mass, 
                               n_charges, last_time_step, **kwargs),
                              bounds=(0, time_step),  options={'xatol':xatol}, method='Bounded')
        x, p, t, field= boris_stepper(xs[-1], ps[-1], ts[-1], mass, n_charges, res.x, **kwargs)
        
        if return_full_track or len(ts)==0:
            xs.append(x)
            ps.append(p)
            ts.append(t)
            fields.append(field)
        else:
            xs[-1] = x
            ps[-1] = p
            ts[-1] = t
            fields[-1] = field

    return xs, ps, ts, fields

